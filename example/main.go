package main

import (
	"container/heap"
	"fmt"
	"math"
)

const (
	A, B, C, D, E, F string = "A", "B", "C", "D", "E", "F"
)

func main() {
	fmt.Print("\nlowest cost node example:\n\n")
	node := createGraph()
	fmt.Printf("Target Key: %v\n\n", F)
	adj := node.GetAdjacentCosts(F)
	fmt.Println("Adjacent Costs...")
	for k, v := range adj {
		fmt.Printf("Key: %v,\tValue: %v\n", k, v)
	}
	fmt.Print("\nParents...\n")
	pars := node.GetParents(F)
	for k, v := range pars {
		par := "None"
		if v != nil {
			par = *v
		}
		fmt.Printf("Key: %v,\tParent: %v\n", k, par)
	}
	fmt.Println("")
}

type Node[T comparable] struct {
	Value T
	edges []*Edge[T]
}

type Edge[T comparable] struct {
	node *Node[T]
	cost float64
}
type Item[T comparable] struct {
	value    T
	priority int
	index    int // index of the item on the heap
}
type PriorityQueue[T comparable] []*Item[T]

// New Node[T] Constructor
func NewNode[T comparable](t T) *Node[T] {
	return &Node[T]{Value: t}
}

// New Edge Constructor
func (n *Node[T]) NewEdge(node ...*Edge[T]) {
	n.edges = append(n.edges, node...)
}

// Return Slice of Edges from n
func (n *Node[T]) Edges() []Edge[T] {
	var edges []Edge[T]
	for _, e := range n.edges {
		edges = append(edges, *e)
	}
	return edges
}

func (n *Node[T]) GetAdjacentCosts(targetKey T) map[T]float64 {
	m := map[T]float64{}
	m[targetKey] = math.Inf(1)
	for _, e := range n.Edges() {
		m[e.node.Value] = e.cost
	}
	return m
}

func (n *Node[T]) GetParents(targetKey T) map[T]*T {
	m := map[T]*T{}
	m[targetKey] = nil
	for _, e := range n.Edges() {
		m[e.node.Value] = &n.Value
	}
	return m
}

func (pq PriorityQueue[T]) Len() int { return len(pq) }

func (pq PriorityQueue[T]) Less(i, j int) bool {
	// We want Pop to give us the highest, not lowest, priority so we use greater than here.
	return pq[i].priority > pq[j].priority
}

func (pq PriorityQueue[T]) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue[T]) Push(x any) {
	n := len(*pq)
	item := x.(*Item[T])
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue[T]) Pop() any {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // avoid memory leak
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

// update modifies the priority and value of an Item in the queue.
func (pq *PriorityQueue[T]) update(item *Item[T], value T, priority int) {
	item.value = value
	item.priority = priority
	heap.Fix(pq, item.index)
}

// returns a rooted tree with weighted edges.
func createGraph() *Node[string] {
	// NodeA will be the root node.
	NodeA := NewNode(A)
	NodeB := NewNode(B)
	NodeC := NewNode(C)
	NodeD := NewNode(D)
	NodeE := NewNode(E)
	NodeF := NewNode(F)
	// Add edges and weights (cost)
	NodeA.NewEdge(&Edge[string]{node: NodeB, cost: 5}, &Edge[string]{node: NodeC, cost: 2})
	NodeB.NewEdge(&Edge[string]{node: NodeD, cost: 4})
	NodeC.NewEdge(&Edge[string]{node: NodeB, cost: 1}, &Edge[string]{node: NodeE, cost: 11})
	NodeD.NewEdge(&Edge[string]{node: NodeE, cost: 6}, &Edge[string]{node: NodeF, cost: 10})
	NodeE.NewEdge(&Edge[string]{node: NodeF, cost: 7})
	return NodeA
}
